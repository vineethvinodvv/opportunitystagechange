public with sharing class OpportunityStageHandler {

     /*
    @ Class name        :   StudentRecordHandler.cls
    @ Created by        :   Vineeth V
    @ Created on        :   10-02-2022
    @ Token/Jira Ticket :   SLIV - 001
    @ Description       :  Method for fetching the fieldSets of the object.
*/

@AuraEnabled
public static List<PickListWrapper> getAllStages(){
    try {
        List<PickListWrapper> lstOptions = new List<PickListWrapper>();

        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> listOfPicklistEntries = fieldResult.getPicklistValues();


    // String objectName = 'Contact';
    // String fieldName ='LeadSource';
    
    // Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
    // Schema.DescribeSObjectResult r = s.getDescribe() ;
    // Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
    // Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
    // List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    // for( Schema.PicklistEntry pickListVal : ple){
    //     System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
    // }    
             
        for( Schema.PicklistEntry objPicklist : listOfPicklistEntries)
        {
            lstOptions.add(new PickListWrapper(objPicklist.getLabel(), objPicklist.getValue())); 
        }      
        System.debug(lstOptions);
        return lstOptions;
    } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
    }
}

public class PickListWrapper
{
    @AuraEnabled public String label {get;set;}
    @AuraEnabled public String apiName {get;set;}
    @AuraEnabled public String value {get;set;}
    PickListWrapper(){}

    PickListWrapper(String label, String value)
    {
        this.label = label;
        this.value = value;
    }
}


@AuraEnabled
public static List<FieldWrapper> getFieldSetMembers(String fieldSetName, String objectName)
{
    fieldSetName = fieldSetName.replaceAll('[^a-zA-Z0-9\\s+]', '');
    fieldSetName = fieldSetName.deleteWhitespace();
    Map<String, OpportunityStage__mdt> mapOfStringToOpportunityStage = OpportunityStage__mdt.getAll();
   
    System.debug('getFieldSetMembers');
    System.debug('fieldSetName  '+fieldSetName);
    System.debug('mapOfStringToOpportunityStage.get(fieldSetName).FieldSet__c '+mapOfStringToOpportunityStage.get(fieldSetName).FieldSet__c);
    System.debug('OpportunityStage__mdt.getInstance(fieldName) '+OpportunityStage__mdt.getInstance(fieldSetName).FieldSet__c);
    System.debug(fieldSetName);
    
    List<FieldWrapper> listOfFieldWrapper = new List<FieldWrapper>();
    Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe(); 
    Schema.SObjectType sObjectTypeObj = globalDescribeMap.get(objectName);
    Schema.DescribeSObjectResult describeSObjectResultObj = sObjectTypeObj.getDescribe();
    Schema.FieldSet fieldSetObj = describeSObjectResultObj.FieldSets.getMap().get(OpportunityStage__mdt.getInstance(fieldSetName).FieldSet__c);
    
    List<Schema.FieldSetMember> fieldSetMemberList = fieldSetObj.getFields(); 

    for (Schema.FieldSetMember objFM : fieldSetMemberList) 
    {
        listOfFieldWrapper.add(new FieldWrapper( objFM.getLabel(), objFM.getFieldPath(), objFM.getRequired(), String.valueOf(objFM.getType()), objFM.getDBRequired()));
    }
    System.debug(listOfFieldWrapper);
    return listOfFieldWrapper;
}  

/*
@ Class name        :   StudentRecordHandler.cls
@ Created by        :   Vineeth V
@ Created on        :   10-02-2022
@ Token/Jira Ticket :   SLIV - 001
@ Description       :   Wrapper class for adding all the objects for creating the fieldset list
*/

public class FieldWrapper
{
    @AuraEnabled public String label {get;set;}
    @AuraEnabled public String apiName {get;set;}
    @AuraEnabled public Boolean isRequired {get;set;}
    @AuraEnabled public String type {get;set;}
    @AuraEnabled public Boolean isDBRequired {get;set;}

    FieldWrapper(){}

    FieldWrapper(String label, String apiName, Boolean isRequired, String type, Boolean isDBRequired)
    {
        this.label = label;
        this.apiName = apiName;
        this.isDBRequired = isDBRequired;
        this.isRequired  = isRequired;
        this.type = type;
    }
}
@AuraEnabled
   public static List<Opportunity> searchForRecord(String opportunityId, String fieldSetName){
       try 
       {
           if(fieldSetName!= null)
           {
               fieldSetName = fieldSetName.replaceAll('[^a-zA-Z0-9\\s+]', '');
               String query = 'SELECT ';
               for(FieldWrapper objFieldWrapper : getFieldSetMembers(fieldSetName.deleteWhitespace(), 'Opportunity')) 
               {
                   query += objFieldWrapper.apiName + ', ';
               }
               String std = '\''+ opportunityId +'\'';
               query += 'Id FROM Opportunity WHERE Id = '+std+'' ;            
               List<Opportunity> listOfOpportunity = Database.query(query);
               System.debug(listOfOpportunity);
               return listOfOpportunity; 
           }
           else
           {
                String std = '\''+ opportunityId +'\'';
                return Database.query('SELECT Name, StageName FROM Opportunity WHERE Id = '+std+'');
           }
       } catch (Exception e) {
           throw new AuraHandledException(e.getMessage());
       }
   }
   
   /*
    @ Class name        :   StudentRecordHandler.cls
    @ Created by        :   Vineeth V
    @ Created on        :   10-02-2022
    @ Token/Jira Ticket :   SLIV - 001
    @ Description       :   Handler Method for Updating record with input from the LWC page to Database
*/
@AuraEnabled
public static Object updateOpportunityRecord(Opportunity opportunityObj){
    try {
        System.debug('opportunityObj '+opportunityObj);
        if(opportunityObj != null)
        {
            Update opportunityObj;
            return new Map<String, String>{'Id'=> opportunityObj.Id, 'Name'=> opportunityObj.Name, 'Stage'=> opportunityObj.StageName};
        }
        else {
            return 'Update Failed';
        } 
    } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
    }
}
}