import { api, LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getAllStages from '@salesforce/apex/OpportunityStageHandler.getAllStages';
import getFieldSetMembers from '@salesforce/apex/OpportunityStageHandler.getFieldSetMembers';
import searchForRecord from '@salesforce/apex/OpportunityStageHandler.searchForRecord';
import updateOpportunityRecord from '@salesforce/apex/OpportunityStageHandler.updateOpportunityRecord';
export default class OpportunityStage extends LightningElement {
    @track listOfFields = [];
    @track listOfPath = [];
    @track listOfData = [];
    @track showOpportunityStage;
    @track opportunityStages;
    @track objOpportunityForUpdate = {};
    @track currentStage;
    @track initialStage;
    @track count = 0;
    @api recordId;

    connectedCallback() {
        this.listOfPathHandler();
        this.searchForRecordHandler()
        this.listOfFields = [];
    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    openCloseHandler() {
        if (this.showOpportunityStage == null) {
            this.showOpportunityStage == true;
        }

        if (this.showOpportunityStage == true) {
            this.showOpportunityStage == false;
        }
        else if (this.showOpportunityStage == false) {
            this.showOpportunityStage == true;
        }

    }


    listOfPathHandler() {
        getAllStages()
            .then(stages => {
                let classList;
                this.count = 0;
                let tempList = [];
                for (let index = 0; index < stages.length; index++) {
                    if (stages[index].value == this.initialStage) {

                        this.count=index;
                    }
                    this.listOfPath.push({
                        value: stages[index].value,
                        label: stages[index].label,
                    })
                }
                
                for (let i = 0; i < this.listOfPath.length; i++) {
                    if (i < this.count) {

                        classList = 'slds-path__item slds-is-complete';
                    }
                    else if(i == this.count)
                    {
                        classList = 'slds-path__item slds-is-current slds-is-active';
                    }
                    else {
                        classList = 'slds-path__item slds-is-incomplete';
                    }
                    tempList.push({
                        value: this.listOfPath[i].value,
                        label: this.listOfPath[i].label,
                        classList: classList
                    })
                }

                this.listOfPath = tempList;
            }).catch(error => {
                console.log(error);
            })
        console.log(this.listOfPath);
    }

    // //method for fetching the fields for input dynamically from Fieldsets. 
    fieldMemberHandler() {
        getFieldSetMembers({
            fieldSetName: this.opportunityStages,
            ObjectName: 'Opportunity'
        })
            .then(response => {
                for (var i = 0; i < response.length; i++) {
                    if (response[i].type == "BOOLEAN") {
                        response[i].type = "checkbox";
                    }
                    else if (response[i].type == "STRING") {
                        response[i].type = "text";
                    }
                    else if (response[i].type == "DOUBLE" || response[i].type == "PERCENT" || response[i].type == "CURRENCY") {
                        response[i].type = "number";
                    }
                    else if (response[i].type == "TEXTAREA") {
                        response[i].textArea = true;
                    }
                    else if (response[i].type == "REFERENCE") {
                        response[i].reference = true;
                    }
                }
                this.listOfFields = response;
                console.log(this.listOfFields, 'this.listOfFields');
            }).catch(error => {
                console.error(error);
            })
        this.searchForRecordHandler();
    }

    opportunityStagesHandler(event) {
        this.opportunityStages = event.currentTarget.dataset.value;
        this.currentStage = true;
        this.showOpportunityStage = true;
        this.listOfFields = [];
        var tempList = [];

        let classList;
        console.log("this.count",this.count);
        for (let index = 0; index < this.listOfPath.length; index++) {

            if(this.count == index){
                classList = 'slds-path__item slds-is-current';
            }
            else if(index < this.count)
            {
                classList = 'slds-path__item slds-is-complete';
            }
            else if (this.listOfPath[index].value == this.opportunityStages) {
                classList = 'slds-path__item slds-is-current slds-is-active';
            } else {
                classList = 'slds-path__item slds-is-incomplete';
                
            }

            tempList.push({
                value: this.listOfPath[index].value,
                label: this.listOfPath[index].label,
                classList: classList
            })

        }
        this.listOfPath = tempList;

        this.fieldMemberHandler();

    }

    searchForRecordHandler() {

        if (this.opportunityStages == null) {
            searchForRecord({
                opportunityId: this.recordId,
                fieldSetName: this.opportunityStages
            }).then(resClass => {
                this.initialStage = resClass[0].StageName;

            }).catch(error => {
                console.error(error);
            })
        }
        else if (this.opportunityStages != null) {
            console.log("this.recordId", this.recordId);
            searchForRecord({
                opportunityId: this.recordId,
                fieldSetName: this.opportunityStages
            })
                .then(res => {
                    this.listOfData = res;
                    this.objOpportunityForUpdate = Object.assign({}, res[0]);
                    for (var i = 0; this.listOfFields.length; i++) {
                        if (res[0].hasOwnProperty(this.listOfFields[i].apiName)) {
                            this.listOfFields[i].val = res[0][this.listOfFields[i].apiName];
                        }
                    }
                }).catch(error => {
                    console.error(error);
                })
            console.log("this.listOfData ", this.listOfData);
        }
    }

}