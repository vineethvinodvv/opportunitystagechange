import { api, LightningElement, track } from 'lwc';
import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import searchForRecord from '@salesforce/apex/OpportunityStageHandler.searchForRecord';
import updateOpportunityRecord from '@salesforce/apex/OpportunityStageHandler.updateOpportunityRecord';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ChildComponent extends NavigationMixin(LightningElement) {

    @api opportunityStage;
    @api listOfData;
    @api listOfFields;
    @api getIdFromParent;
    @track objOpportunityForUpdate = {};
    @track objListOfFieldsForCancel = {};
    @track showOpportunityStage = true;
    @track buttonLabel;

    connectedCallback() {
        this.buttonLabel = 'Edit';
        this.disabled = true;
        console.log(this.listOfFields, 'listOfFields');
        console.log(this.listOfData, "listOfData");
    }


    inputHandler(event) {
        let fieldName = event.currentTarget.name;
        console.log("fieldName", fieldName);
        console.log("event.target.value ", event.target.value);

        if (event.target.type == 'checkbox') {
            this.objOpportunityForUpdate[fieldName] = event.target.checked;
        }

        else {
            this.objOpportunityForUpdate[fieldName] = event.target.value;
        }

        this.template.querySelectorAll('lightning-input').forEach(element => {
            if (element.type === 'date' && event.target.value == null) {
                element.value = '';
                element.setCustomValidity('');
                element.reportValidity();
            }

            if (element.name === 'ExpectedRevenue' ) {
                element.setCustomValidity('');
                element.reportValidity();
            }

        });

        console.log(this.objOpportunityForUpdate)
    }

    buttonHandler(event) {

        if (event.target.label == "Save") {
            this.disabled = true;
            this.buttonLabel = "Edit"
            this.objOpportunityForUpdate.Id = this.getIdFromParent;
            this.objOpportunityForUpdate.StageName = this.opportunityStage;
            this.updateOpportunityRecordHandler();
        }
        else if (event.target.label == "Edit") {
            this.disabled = false;
            this.buttonLabel = "Save";
        }

    }

    updateOpportunityRecordHandler() {

        updateOpportunityRecord({ opportunityObj: this.objOpportunityForUpdate })
            .then(updateRes => {
                if (updateRes.Id != null) {
                    this.showNotification('Success', `Opportunity Updated Successfully `, 'success');
                    this.showOpportunityStage = false;
                    this.navigateHandler(this.getIdFromParent);
                }
                else {
                    this.showNotification('Error', "Failed Update", 'error');
                }
            }).catch(error => {
                this.showNotification('Error', error.body.message, 'error');
                console.error(error);
            });
    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    searchForRecordHandler() {
        searchForRecord({
            opportunityId: this.getIdFromParent,
            fieldSetName: this.opportunityStage
        })
            .then(res => {
                for (var i = 0; this.listOfFields.length; i++) {
                    if (res[0].hasOwnProperty(this.listOfFields[i].apiName)) {
                        this.listOfFields[i].val = res[0][this.listOfFields[i].apiName];
                    }
                }
            }).catch(error => {
                console.error(error);
            })

    }

    resetHandler(event) {
        //this.searchForRecordHandler();
        eval("$A.get('e.force:refreshView').fire();");
       console.log("this.listOfData[0]",JSON.parse(JSON.stringify(this.listOfData[0])));
        let copyOfListofFields = JSON.parse(JSON.stringify(this.listOfFields));

        for (let i = 0; i < copyOfListofFields.length; i++) {
            if (this.listOfData[0].hasOwnProperty(copyOfListofFields[i].apiName)) {

                copyOfListofFields[i].val = this.listOfData[0][copyOfListofFields[i].apiName];
            } else {
                copyOfListofFields[i].val = '';
            }
        }
        this.listOfFields = copyOfListofFields;

        this.template.querySelectorAll('lightning-input').forEach(element => {
            element.value = this.listOfData[0].hasOwnProperty(element.name) ? this.listOfData[0][element.name] : '';

            if (element.type == 'checkbox') {
                element.checked = this.listOfData[0][element.name];
            }
            if (element.type == 'email') {
                element.setCustomValidity('');
                element.reportValidity();
            }
        });
        this.objOpportunityForUpdate = JSON.parse(JSON.stringify(this.listOfData[0]));
        console.log( this.objOpportunityForUpdate, " this.objOpportunityForUpdate");
        this.buttonLabel = "Edit";
        this.disabled = true;

    }

    navigateHandler(recId) {
                this.accountHomePageRef = {
                    type: 'standard__recordPage',
                  attributes: {
               recordId: recId,
                       objectApiName: 'Opportunity',
                        actionName: 'view'
                    }
                }
                this[NavigationMixin.Navigate](this.accountHomePageRef);
            }
        
        

}